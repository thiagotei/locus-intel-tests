#include <random>
#include <algorithm>
#include <iostream>
#include <vector>
#include <assert.h>
#include "numerics.hpp"
#include "cxxopts.hpp"

extern "C" {
    int dgemm_kernel(long, long, long, double, double*, double*, double* , long);
};

template<typename T>
struct matrix
{
  int rows_;
  int cols_;
  std::vector<T> data_;

  explicit matrix(int n, int m):rows_(n), cols_(m), data_(n*n){}
  matrix(const matrix<T>& in)=default;

  T* data() { return data_.data();}
  T* begin() { return data_.begin();}
  T* end() { return data_.end();}
  inline int rows() { return rows_;}
  inline int columns() { return cols_;}
  inline int size() { return rows_*cols_;}
  inline T& operator()(int i, int j) { return data_[i*cols_+j];}
  inline T operator()(int i, int j) const { return data_[i*cols_+j];}
};

template <typename scalar_type>
int lu_decompose(matrix<scalar_type>& input, lapack_int* perm) {
  assert(input.rows() == input.columns());
  int n=input.rows();
  std::iota(perm, perm+n, 0);
  matrix<scalar_type> input1(input);

  for (size_t j = 0; j < n; ++j) {
    size_t max_index = j;
    scalar_type max_value = 0;
    for (size_t i = j; i < n; ++i) {
      scalar_type value = std::abs(input1(perm[i], j));
      if (value > max_value) {
        max_index = i;
        max_value = value;
      }
    }
    if (max_value <= std::numeric_limits<scalar_type>::epsilon())
      throw std::runtime_error("matrix is singular");
    if (j != max_index)
      std::swap(perm[j], perm[max_index]);
    size_t jj = perm[j];
    for (size_t i = j + 1; i < n; ++i) {
      size_t ii = perm[i];
      input1(ii, j) /= input1(jj, j);
      for (size_t k = j + 1; k < n; ++k)
        input1(ii, k) -= input1(ii, j) * input1(jj, k);
    }
  }

  for (size_t j = 0; j < n; ++j) {
    input(j, j) = 1;
    for (size_t i = j + 1; i < n; ++i)
      input(i, j) = input1(perm[i], j);
    for (size_t i = 0; i <= j; ++i)
      input(i, j) = input1(perm[i], j);
  }

  return 0;
}


int main(int argc, char** argv)
{
  using namespace numerics;

  cxxopts::Options options("lu", "Miniapp running LU factorization for performance analysis.");
  options.add_options()
    ("h,help", "Print help")
    ("s,size", "Problem Size", cxxopts::value<int>()->default_value("256"))
    ("r,repetitions", "Number of repetitions of algorithm runs", cxxopts::value<int>()->default_value("5"))
    ;
  auto cmd_input = options.parse(argc, argv);

  if (cmd_input.count("help")) 
  {
    std::cout << options.help() << std::endl;
    exit(0);
  }


  int N=cmd_input["size"].as<int>();
  int LDA=N;
  int repetitions=cmd_input["repetitions"].as<int>();
  using real_type=double;


  Random<real_type> rng(-0.5,0.5);

  std::vector<real_type> A(N*N), Ac(N*N),WorkSpace;
  std::vector<lapack_int> Pivot(N); 

  //Fill A with random numbers
  std::generate_n(A.data(),A.size(),rng);

  matrix<real_type> A2(N,N);
  std::vector<lapack_int> Pivot2(N); 
  for(int i=0; i<N; ++i)
    for(int j=0; j<N; ++j)
      A2(i,j)=A[i+j*N];

  //Copy for checking
  std::copy(A.begin(),A.end(),Ac.begin());

  //LU factorzation uisng LAPACK
  auto status=getrf(N, N, A.data(), LDA, Pivot.data());
  if (status != 0)
  {
    std::ostringstream msg;
    msg << "getrf failed with error " << status << std::endl;
    throw std::runtime_error(msg.str());
  }

  status=lu_decompose(A2, Pivot2.data());
  double diff=0.0;
  for(int i=0; i<N; ++i)
    for(int j=0; j<N; ++j)
      diff += std::abs(A[i+j*N]-A2(i,j));
  std::cout << "Diff = " << diff << std::endl;

  for (int rep_it=0; rep_it<repetitions; rep_it++) 
  {
    std::copy(Ac.begin(),Ac.end(),A2.data());
    auto st=lu_decompose(A2, Pivot2.data());
  }

  return 0;
}
