topdir=`pwd`

mkdir -p OpenBLAS
mkdir -p install build

if [ ! -d OpenBLAS/original ]; then
  git clone https://github.com/xianyi/OpenBLAS.git OpenBLAS/original
fi

if [ ! -d OpenBLAS/patched ]; then
  git clone https://github.com/xianyi/OpenBLAS.git OpenBLAS/patched
  cd OpenBLAS/patched
  git apply ../../patches/OpenBlas.patch
  cd ../..
fi

build () {
  c_compiler=$1
  cpp_compiler=$2
  open_blas_type=$3

  blas_src=${topdir}/OpenBLAS/${open_blas_type}

  echo $c_compiler
  echo $cpp_compiler

  build_dir=build/openblas-${c_compiler}-${open_blas_type}
  install_dir=${topdir}/install/openblas-${c_compiler}-${open_blas_type}

  if [ -d ${install_dir} ]; then
    return
  fi

  mkdir -p ${build_dir}
  pushd  ${build_dir}
#  CXX=${cpp_compiler} CC=${c_compiler} cmake ${blas_src}  -DCMAKE_INSTALL_PREFIX=${install_dir} -DCMAKE_CXX_FLAGS='-g' -DCMAKE_C_FLAGS='-g' -DTARGET='generic'
  CXX=${cpp_compiler} CC=${c_compiler} TARGET=generic TARGET_CORE=generic cmake ${blas_src}  -DCMAKE_INSTALL_PREFIX=${install_dir} -DCMAKE_CXX_FLAGS='-g' -DCMAKE_C_FLAGS='-g' -DCMAKE_Fortran_FLAGS='-g' -DCMAKE_ASM_FLAGS='-g'
  #cmake --build . --parallel
  TARGET=generic TARGET_CORE=generic cmake --build .
  make install
  popd 

  export LIBRARY_PATH=${install_dir}/lib:$LIBRARY_PATH
  export LD_LIBRARY_PATH=${install_dir}/lib:$LD_LIBRARY_PATH
}

build icc icpc original
#build icx icpx original
#build icc icpc patched
#build icx icpx patched


