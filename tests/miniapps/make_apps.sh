topdir=`pwd`
blas_src=${topdir}/OpenBLAS

build() {
  c_compiler=$1
  open_blas_type=$2

  install_dir=${topdir}/install/openblas-${c_compiler}-${open_blas_type}
  build_dir=build/apps-${c_compiler}-${open_blas_type}

  #OpenBLAS_ROOT=${OpenBLAS_ROOT:-"${install_dir}"} 
  OpenBLAS_ROOT=${install_dir}

  if [[ ! "$LIBRARY_PATH" =~ "$OpenBLAS_ROOT" ]]
  then
    export LIBRARY_PATH=${OpenBLAS_ROOT}/lib:$LIBRARY_PATH
    export LD_LIBRARY_PATH=${OpenBLAS_ROOT}/lib:$LD_LIBRARY_PATH
  fi


  mkdir -p ${build_dir}
  pushd ${build_dir}
  CXX=icpx CC=icx cmake ${topdir}/src -DOpenBLAS_ROOT=${OpenBLAS_ROOT} -DCMAKE_CXX_FLAGS='-g' -DCMAKE_C_FLAGS='-g'
  #cmake --build . --parallel
  cmake --build . 
  popd
}


#build icx original
build icc original
#build icx patched
#build icc patched
