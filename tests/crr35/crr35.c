#include <stdio.h>
#include <stdlib.h>
#include <pips_runtime.h>

#define matSize 2048L 
#define maxTest 3
#define alpha 1
#define beta 1


#define bhtype double
//typedef double bhtype;
bhtype matA[matSize][matSize];
bhtype matB[matSize][matSize];
bhtype matC[matSize][matSize];

/* Statically allocate the matrices.  This can improve the compiler's
   ability to optimize operations on these variables.  This will be discussed
   in more detail later in the course. */
//bhtype matA[matSize*matSize], matB[matSize*matSize], matC[matSize*matSize];

double mysecond(void);
void dummy(bhtype *a, bhtype *b, bhtype *c);

double data[1400 + 0][1200 + 0];

void loop()
{
  int i= 0;
  int j = 0;;

  int __i_0__ = i;
  int __j_1__ = j;
#pragma @ICE loop=crr35loop
    for (__i_0__ = 0; __i_0__ <= 1399; __i_0__ += 4) {
      for (__j_1__ = 0; __j_1__ <= 1199; __j_1__ += 1) {
        data[__i_0__][__j_1__] = ((double )(__i_0__ * __j_1__)) / ((double )1200) + ((double )__i_0__);
        data[__i_0__ + 1][__j_1__] = ((double )((__i_0__ + 1) * __j_1__)) / ((double )1200) + ((double )(__i_0__ + 1));
        data[__i_0__ + 2][__j_1__] = ((double )((__i_0__ + 2) * __j_1__)) / ((double )1200) + ((double )(__i_0__ + 2));
        data[__i_0__ + 3][__j_1__] = ((double )((__i_0__ + 3) * __j_1__)) / ((double )1200) + ((double )(__i_0__ + 3));
      }
    }
  
}

void matmul(bhtype matA[matSize][matSize], bhtype matB[matSize][matSize], bhtype matC[matSize][matSize])
{
int i, j, k;
		
#pragma @ICE loop=matmulOld
for (i=0; i<matSize; i++)
	for (k=0; k<matSize; k++)
		for (j=0; j<matSize; j++) 
			//matC[i][j] += matA[i][k] * matB[k][j];
			matC[i][j] = beta*matC[i][j] +  alpha*matA[i][k] * matB[k][j];

}


int main(int argc, char *argv[])
{
    double tStart, tEnd, tLoop, rate, t;
    int     i,j, k,tests;
//    matA =  malloc(matSize*matSize*sizeof(bhtype));
//    matB =  malloc(matSize*matSize*sizeof(bhtype));
//    matC =  malloc(matSize*matSize*sizeof(bhtype));

    /* Initialize the matrics */
    /* Note that this is *not* in the best order with respect to cache;
       this will be discussed later in the course. */
    for (i=0; i<matSize; i++)
	for (j=0; j<matSize; j++) {
	    matA[i][j] = 1.0 + i;
	    matB[i][j] = 1.0 + j;
	    matC[i][j] = 0.0;
	}

    tLoop = 1.0e10;
    for (tests=0; tests<maxTest; tests++) {
	tStart = mysecond();
//	matmul(matA, matB, matC);
#pragma @ICE loop=matmul
	for (i=0; i<matSize; i++)
		for (k=0; k<matSize; k++)
			for (j=0; j<matSize; j++) 
				matC[i][j] = beta*matC[i][j] +  alpha*matA[i][k] * matB[k][j];

	tEnd = mysecond();
	t = tEnd - tStart;
	//dummy(matA, matB, matC);
	if (t < tLoop) tLoop = t;
	if (matC[0][0] < 0) {
	    fprintf(stderr, "Failed matC sign test\n");
	}
    }

    /* Note that explicit formats are used to limit the number of
       significant digits printed (at most this many digits are significant) */
    printf("Matrix size = %ld | ", matSize);
    printf("Time        = %7.5lf ms | ", tLoop * 1.0e3);
    rate = (2.0 * matSize) * matSize * (matSize / tLoop);
    printf("Rate        = %.2e MFLOP/s\n", rate * 1.0e-6);

//    free(matA);
//    free(matB);
//    free(matC);

    return 0;
}
