#/bin/bash

echo "Checking out Capescripts"
git lfs clone git@gitlab.com:davidwong/cape-experiment-scripts.git

cd cape-experiment-scripts
./setup.sh
cd ..


echo "Building tests"

pushd tests/miniapps/
./make_openblas.sh
./make_apps.sh      
popd
echo "binary executables are under ./tests/miniapps/build/apps-*/bin/"

# Add more to build more tests

echo "Container can be started by running run-container.sh under $(pwd)/cape-experiments-scripts/container"
echo "e.g. ./cape-experiment-scripts/container/run-container.sh"
echo "Locus and other utilites are under /share folder in container"
